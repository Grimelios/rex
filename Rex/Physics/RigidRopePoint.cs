﻿using System.Diagnostics;
using Engine.Interfaces._3D;
using GlmSharp;

namespace Rex.Physics
{
	[DebuggerDisplay("P={Position}, V={Velocity}, M={Mass}")]
	public class RigidRopePoint : IPositionable3D
	{
		private float mass;

		public RigidRopePoint(vec3 p, float mass)
		{
			Position = p;
			Mass = mass;
		}

		public vec3 Position { get; set; }
		public vec3 Velocity { get; set; }

		public float InverseMass { get; set; }
		public float Mass
		{
			get => mass;
			set
			{
				Debug.Assert(value > 0, "Rigid rope point mass must be positive.");

				mass = value;
				InverseMass = 1 / value;
			}
		}

		public bool IsFixed { get; set; }
	}
}
