﻿using System;
using Engine;
using Engine.Shapes._3D;
using Engine.Utility;
using GlmSharp;

namespace Rex.Physics
{
	public static class PhysicsHelper
	{
		public static bool Detect(Box box, vec3 p, vec3 v, float radius, out vec3 result)
		{
			var center = box.Position;
			p -= center;
			
			if (box.IsOrientable)
			{
				p = box.InverseOrientation * p;
			}

			if (box.Contains(p, Coordinates.Relative))
			{
				result = DetectInner(box, p, v, radius);

				return true;
			}

			var w = box.Width / 2;
			var h = box.Height / 2;
			var d = box.Depth / 2;
			var x = p.x > w ? w : (p.x < -w ? -w : 0);
			var y = p.y > h ? h : (p.y < -h ? -h : 0);
			var z = p.z > d ? d : (p.z < -d ? -d : 0);

			float distance;
			vec3 n;
			bool isNormalizationRequired = true;

			// A vertex is closest.
			if (x != 0 && y != 0 && z != 0)
			{
				var vertex = new vec3(x, y, z);
				distance = Utilities.DistanceSquared(p, vertex);
				n = p - vertex;
			}
			// A face is closest.
			else if (x == 0 && y == 0 && z != 0)
			{
				distance = Math.Abs(p.z - z);
				n = new vec3(0, 0, Math.Sign(p.z - z));
				isNormalizationRequired = false;
			}
			else if (x == 0 && y != 0 && z == 0)
			{
				distance = Math.Abs(p.y - y);
				n = new vec3(0, Math.Sign(p.y - y), 0);
				isNormalizationRequired = false;
			}
			else if (x != 0 && y == 0 && z == 0)
			{
				distance = Math.Abs(p.x - x);
				n = new vec3(Math.Sign(p.x - x), 0, 0);
				isNormalizationRequired = false;
			}
			else
			{
				// An edge is closest.
				vec3 p0;
				vec3 p1;

				if (x != 0)
				{
					if (y != 0)
					{
						p0 = new vec3(x, y, d);
						p1 = new vec3(x, y, -d);
					}
					else
					{
						p0 = new vec3(x, h, z);
						p1 = new vec3(x, -h, z);
					}
				}
				else
				{
					p0 = new vec3(w, y, z);
					p1 = new vec3(-w, y, z);
				}

				distance = Utilities.DistanceSquaredToLine(p, p0, p1, out var t);
				n = p - (p0 + (p1 - p0) * t);
			}

			if ((isNormalizationRequired && distance < radius * radius) ||
				(!isNormalizationRequired && distance < radius))
			{
				if (isNormalizationRequired)
				{
					distance = (float)Math.Sqrt(distance);
					n = Utilities.Normalize(n);
				}

				result = n * (radius - distance);

				if (box.IsOrientable)
				{
					result = box.Orientation * result;
				}

				return true;
			}

			result = vec3.Zero;

			return false;
		}

		private static vec3 DetectInner(Box box, vec3 p, vec3 v, float radius)
		{
			var w = box.Width / 2;
			var h = box.Height / 2;
			var d = box.Depth / 2;	
			var xV = new vec3(v.x > 0 ? -w - p.x - radius : w - p.x + radius, 0, 0);
			var yV = new vec3(0, v.y > 0 ? -h - p.y - radius : h - p.y + radius, 0);
			var zV = new vec3(0, 0, v.z > 0 ? -d - p.z - radius : d - p.z + radius);

			// To determine which face was hit first, ratios are compared.
			var xy = Math.Abs(xV.x / yV.y);
			var xz = Math.Abs(xV.x / zV.z);
			var yz = Math.Abs(yV.y / zV.z);
			var xyV = v.y == 0 ? float.PositiveInfinity : Math.Abs(v.x / v.y);
			var xzV = v.z == 0 ? float.PositiveInfinity : Math.Abs(v.x / v.z);
			var yzV = v.z == 0 ? float.PositiveInfinity : Math.Abs(v.y / v.z);

			// X was hit before Y (or at the same time).
			if (xyV >= xy)
			{
				return xzV > xz ? xV : zV;
			}
			
			return yzV > yz ? yV : zV;
		}
	}
}
