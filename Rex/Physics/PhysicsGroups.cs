﻿using System;

namespace Rex.Physics
{
	[Flags]
	public enum PhysicsGroups
	{
		Environment = 1<<0,
		Player = 1<<1
	}
}
