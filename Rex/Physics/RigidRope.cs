﻿using System;
using Engine;
using Engine.Interfaces;
using Engine.Interfaces._2D;
using Engine.Utility;
using GlmSharp;

namespace Rex.Physics
{
	public class RigidRope : IDynamic
	{
		private RigidRopePoint[] points;

		private float segmentLength;
		private float[] accumulatedImpulses;

		public RigidRope(float segmentLength, vec3[] points)
		{
			this.segmentLength = segmentLength;
			this.points = new RigidRopePoint[points.Length];

			for (int i = 0; i < points.Length; i++)
			{
				this.points[i] = new RigidRopePoint(points[i], 1);
			}

			accumulatedImpulses = new float[points.Length - 1];
		}

		public RigidRopePoint[] Points => points;

		public void Update()
		{
			const float Bias = 0.1f;
			const float Softness = 0.01f;

			// Move points.
			foreach (var p in points)
			{
				if (!p.IsFixed)
				{
					p.Velocity += new vec3(0, -20, 0) * Game.DeltaTime;
					p.Position += p.Velocity * Game.DeltaTime;
				}
			}

			// Pre-solve.
			var jacobian = new vec3[points.Length * 4];
			var effectiveMasses = new float[points.Length - 1];
			var biases = new float[points.Length - 1];

			for (int i = 0; i < points.Length - 1; i++)
			{
				var point0 = points[i];
				var point1 = points[1];

				if (point0.IsFixed && point1.IsFixed)
				{
					continue;
				}

				var p0 = point0.Position;
				var p1 = point1.Position;
				var dP = p1 - p0;
				var d = Utilities.LengthSquared(dP);

				if (d <= segmentLength * segmentLength)
				{
					continue;
				}

				d = (float)Math.Sqrt(d);

				var n = dP / d;
				var index = i * 4;

				d -= segmentLength;

				jacobian[index] = -n;
				jacobian[index + 1] = vec3.Zero;
				jacobian[index + 2] = n;
				jacobian[index + 3] = vec3.Zero;

				var m = point0.InverseMass + point1.InverseMass;
				m += Softness / Game.DeltaTime;
				m = 1 / m;

				effectiveMasses[i] = m;
				biases[i] = d * Bias * (1 / Game.DeltaTime);

				var impulse = accumulatedImpulses[i];

				if (!point0.IsFixed)
				{
					point0.Velocity += point0.InverseMass * impulse * jacobian[index];
				}

				if (!point1.IsFixed)
				{
					point1.Velocity += point1.InverseMass * impulse * jacobian[index + 2];
				}
			}

			// Solve.
			for (int i = 0; i < points.Length - 1; i++)
			{
				var point0 = points[i];
				var point1 = points[i + 1];

				if (point0.IsFixed && point1.IsFixed)
				{
					continue;
				}

				var index = i * 4;
				var v = Utilities.Dot(point0.Velocity, jacobian[index]) + Utilities.Dot(point1.Velocity,
					jacobian[index + 2]);
				var impulse = accumulatedImpulses[i];
				var softness = impulse * Softness / Game.DeltaTime;
				var lambda = -effectiveMasses[i] * (v + Bias + softness);

				accumulatedImpulses[i] = Math.Min(impulse + lambda, 0);
				lambda = accumulatedImpulses[i] - impulse;

				if (!point0.IsFixed)
				{
					point0.Velocity += point0.InverseMass * lambda * jacobian[index];
				}

				if (!point1.IsFixed)
				{
					point1.Velocity += point1.InverseMass * lambda * jacobian[index + 2];
				}
			}
		}
	}
}
