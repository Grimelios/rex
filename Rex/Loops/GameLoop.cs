﻿using System.Collections.Generic;
using Engine.Graphics._2D;
using Engine.Interfaces;
using Engine.Interfaces._3D;
using Engine.UI;
using Engine.View;

namespace Rex.Loops
{
	public abstract class GameLoop : IDynamic
	{
		protected List<IRenderTargetUser3D> renderTargetUsers3D = new List<IRenderTargetUser3D>();

		public Camera3D Camera { get; set; }
		public Canvas Canvas { get; set; }
		public SpriteBatch SpriteBatch { get; set; }

		public abstract void Initialize();
		public abstract void Update();
		public abstract void Draw(float t);

		public virtual void DrawTargets(float t)
		{
			renderTargetUsers3D.ForEach(r => r.DrawTargets(t));
		}
	}
}
