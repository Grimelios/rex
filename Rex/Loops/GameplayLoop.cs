﻿using Engine;
using Engine.Core;
using Engine.Core._3D;
using Engine.Editing;
using Engine.Entities;
using Engine.Utility;
using Engine.View;
using GlmSharp;
using Jitter;
using Jitter.Collision;
using Jitter.LinearMath;
using Rex.Entities;
using Rex.Entities.Player;
using Rex.Entities.Tools;
using Rex.View;

namespace Rex.Loops
{
	public class GameplayLoop : GameLoop
	{
		private World world;
		private Scene scene;
		private TimeOfDay timeOfDay;

		public override void Initialize()
		{
			Canvas.Load("Canvas.json");

			var terminal = Canvas.GetElement<Terminal>();
			terminal.Add(new FreecamCommand(Camera, 15));

			var firstPersonView = new FirstPersonView();
			Camera.Attach(firstPersonView);

			var system = new CollisionSystemSAP();
			world = new World(system);
			world.Gravity = new JVector(0, -20, 0);

			timeOfDay = new TimeOfDay();
			scene = new Scene
			{
				Camera = Camera,
				Canvas = Canvas,
				World = world
			};

			var pocketWatch = new PocketWatch(timeOfDay);
			pocketWatch.SetPosition(new vec3(-1.1f, 0, 0), false);

			var ancientWatch = new AncientWatch(timeOfDay);
			ancientWatch.SetPosition(new vec3(1.1f, 0, 0), false);
			ancientWatch.SetOrientation(quat.FromAxisAngle(Constants.PiOverTwo, vec3.UnitY), false);

			var map = ContentCache.GetMesh("Map.obj");
			var player = new PlayerCharacter(timeOfDay, firstPersonView);
			player.Map = map;

			var renderer = scene.Renderer;
			renderer.Light.Direction = Utilities.Normalize(new vec3(1, -0.25f, 0));
			//renderer.Add(new Model(map));

			//scene.Add(pocketWatch);
			//scene.Add(ancientWatch);
			scene.Add(player);
			renderTargetUsers3D.Add(scene.Renderer);
		}

		public override void Update()
		{
			timeOfDay.Update();
			world.Step(Game.DeltaTime, false);
			scene.Update();
			Camera.Update();
		}

		public override void Draw(float t)
		{
			scene.Draw(Camera, t);

			var primitives = scene.Primitives;

			primitives.DrawLine(-scene.Renderer.Light.Direction * 3, vec3.Zero, Color.Gold);
			primitives.Flush();
		}
	}
}
