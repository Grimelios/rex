﻿using System.Collections.Generic;
using Engine;
using Engine.Core._2D;
using Engine.Graphics._2D;
using Engine.Messaging;
using Engine.UI;
using Engine.View;
using GlmSharp;
using Rex.Loops;
using static Engine.GL;
using static Engine.GLFW;

namespace Rex
{
	public class MainGame : Game
	{
		private Camera3D camera;
		private Canvas canvas;
		private GameLoop activeLoop;
		private SpriteBatch sb;
		private RenderTarget mainTarget;
		private Sprite mainSprite;

		// TODO: Should pause functionality be moved to the gameplay loop?
		private bool isPaused;
		private bool isPauseToggled;
		private bool justPaused;

		// When the game is paused, the current interpolation value needs to be stored (to prevent visible jitter while
		// paused).
		private float tPaused;

		public MainGame(ivec2? windowPosition = null) : base("Rex", "Rex", windowPosition)
		{
			glClearColor(0, 0, 0, 1);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glPrimitiveRestartIndex(Constants.RestartIndex);
			glfwSetInputMode(window.Address, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

			camera = new Camera3D();
			canvas = new Canvas(this);
			sb = new SpriteBatch();
			PauseElements = new List<CanvasElement>();

			mainTarget = new RenderTarget(Resolution.RenderWidth, Resolution.RenderHeight, RenderTargetFlags.Color |
				RenderTargetFlags.Depth);
			mainSprite = new Sprite(mainTarget, Alignments.Left | Alignments.Top);
			mainSprite.Mods = SpriteModifiers.FlipVertical;

			activeLoop = CreateLoop(Gamestates.Gameplay);

			MessageSystem.Subscribe(this, CoreMessageTypes.ResizeWindow, data =>
			{
				mainSprite.ScaleTo(Resolution.WindowWidth, Resolution.WindowHeight, false);
			});

			// Calling this function here is required to ensure that all classes receive initial resize messages.
			MessageSystem.ProcessChanges();
			MessageSystem.Send(CoreMessageTypes.ResizeRender, Resolution.RenderDimensions);
			MessageSystem.Send(CoreMessageTypes.ResizeWindow, Resolution.WindowDimensions);
		}

		// While the game is paused, the entire game is effectively frozen (including most UI elements), *except* for
		// elements in this list. Similarly, for rendering, interpolation is frozen (except for this list).
		public List<CanvasElement> PauseElements { get; }

		private GameLoop CreateLoop(Gamestates state)
		{
			GameLoop loop = null;

			switch (state)
			{
				case Gamestates.Gameplay:
					loop = new GameplayLoop();
					break;

				case Gamestates.Title:
					//loop = new TitleLoop();
					break;

				case Gamestates.Splash:
					//loop = new SplashLoop();
					break;
			}

			loop.Camera = camera;
			loop.Canvas = canvas;
			loop.SpriteBatch = sb;
			loop.Initialize();

			return loop;
		}

		protected override void Update()
		{
			if (isPaused)
			{
				PauseElements.ForEach(e => e.Update());
			}
			else
			{
				activeLoop.Update();
				canvas.Update();
			}
			
			// TODO: Verify ordering between pausing and gamestate changes.
			// Pausing incurs an intentional one-frame buffer. On the frame the game is paused, the game still updates
			// one additional time. Similarly, on the frame the game is unpaused, the game doesn't begin updating again
			// until the *next* frame.
			if (isPauseToggled)
			{
				// The toggle flag is set back to false in the Draw call (in order to properly capture tPaused).
				isPaused = !isPaused;
				isPauseToggled = false;
				justPaused = isPaused;

				// TODO: Hide the cursor until the mouse is actually used (similar to Ori, which hides the cursor most of the time while using a controller).
				// The cursor becomes visible while in a menu.
				ToggleCursor(isPaused);
			}

			MessageSystem.ProcessChanges();
		}

		protected override void Draw(float t)
		{
			if (justPaused)
			{
				tPaused = t;
				justPaused = false;
			}

			var tActual = t;

			if (isPaused)
			{
				t = tPaused;
			}

			// Render 3D targets.
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glDepthFunc(GL_LEQUAL);

			camera.Recompute(t);
			activeLoop.DrawTargets(t);
			mainTarget.Apply();
			activeLoop.Draw(t);

			// Render 2D targets.
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
			glDepthFunc(GL_NEVER);

			canvas.DrawTargets(sb, t);

			// Draw to the main screen.
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, (uint)Resolution.WindowWidth, (uint)Resolution.WindowHeight);

			sb.ApplyTarget(null);
			mainSprite.Draw(sb, t);
			canvas.Draw(sb, t);

			if (isPaused)
			{
				// TODO: Handle render targets for pause elements (if needed for any of them).
				// While paused, pause-related UI elements (like the pause menu) should still be interpolated
				// properly (rather than frozen like everything else).
				PauseElements.ForEach(e => e.Draw(sb, tActual));
			}

			sb.Flush();
		}
	}
}
