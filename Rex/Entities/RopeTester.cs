﻿using Engine;
using Engine.Input;
using Engine.Input.Data;
using GlmSharp;
using static Engine.GLFW;

namespace Rex.Entities
{
	public class RopeTester
	{
		public RopeTester(Rope rope)
		{
			rope.InnerRope.Points[0].Position = new vec3(0, 3, 0);

			InputProcessor.Add(data =>
			{
				const float Speed = 20;

				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				bool left = keyboard.Query(GLFW_KEY_KP_4, InputStates.Held);
				bool right = keyboard.Query(GLFW_KEY_KP_6, InputStates.Held);
				bool forward = keyboard.Query(GLFW_KEY_KP_8, InputStates.Held);
				bool back = keyboard.Query(GLFW_KEY_KP_5, InputStates.Held);
				bool up = keyboard.Query(GLFW_KEY_KP_7, InputStates.Held);
				bool down = keyboard.Query(GLFW_KEY_KP_9, InputStates.Held);

				var point = rope.InnerRope.Points[0];
				var p = point.Position;

				if (left ^ right)
				{
					p.x += (left ? -1 : 1) * Speed * Game.DeltaTime;
				}

				if (forward ^ back)
				{
					p.z += (forward ? -1 : 1) * Speed * Game.DeltaTime;
				}

				if (up ^ down)
				{
					p.y += (up ? -1 : 1) * Speed * Game.DeltaTime;
				}

				point.Position = p;
			});
		}
	}
}
