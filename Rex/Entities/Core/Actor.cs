﻿using Engine.Entities;
using GlmSharp;

namespace Rex.Entities.Core
{
	public abstract class Actor : LivingEntity
	{
		protected Actor(uint group) : base(group)
		{
		}
	}
}
