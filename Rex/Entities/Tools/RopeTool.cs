﻿using System;
using Engine;
using Engine.Input;
using Engine.Input.Data;
using Engine.Utility;
using GlmSharp;
using Rex.Entities.Player;

namespace Rex.Entities.Tools
{
	public class RopeTool : Tool
	{
		private Rope rope;

		private bool isEnabled;

		public RopeTool(Rope rope)
		{
			this.rope = rope;

			InputProcessor.Add(data =>
			{
				if (!data.TryGetData(out KeyboardData keyboard))
				{
					return;
				}

				if (keyboard.Query(GLFW.GLFW_KEY_R, InputStates.PressedThisFrame))
				{
					isEnabled = true;
				}
			});
		}

		public void Refresh(PlayerCharacter player)
		{
			if (!isEnabled)
			{
				return;
			}

			/*
			var inner = rope.InnerRope;
			var points = inner.Points;
			var heldIndex = 0;

			for (int i = 1; i < points.Length; i++)
			{
				if (points[i].IsFixed)
				{
					heldIndex = i;

					break;
				}
			}

			var d = Utilities.DistanceSquared(points[heldIndex].Position, points[heldIndex - 1].Position);
			var target = inner.SegmentLength;

			if (d > (target + 0.1f) * (target + 0.1f))
			{
				points[heldIndex].IsFixed = false;
				d = (float)Math.Sqrt(d);

				while (d > target && heldIndex < points.Length -1)
				{
					d -= target;
					heldIndex++;
				}
			}

			inner.VisiblePointCount = heldIndex;

			var held = points[heldIndex];
			held.IsFixed = true;
			held.Position = player.Position + quat.FromAxisAngle(-player.Yaw, vec3.UnitY) * new vec3(0, 1.25f, -0.5f);
			*/
		}
	}
}
