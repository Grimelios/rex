﻿using Engine;
using Engine.Core._2D;
using Engine.Core._3D;
using Engine.Entities;
using Engine.Props;
using Engine.Timing;
using Engine.Utility;
using GlmSharp;
using Newtonsoft.Json.Linq;

namespace Rex.Entities.Tools
{
	public class AncientWatch : Tool
	{
		private TimeOfDay timeOfDay;
		private Model outerRing;
		private Sprite3D mainFace;
		private Sprite3D phaseFace;
		private SingleTimer hourSnapTimer;

		private int oldHour;

		private float hourSnapStart;
		private float hourSnapEnd;
		private float hourAngle;

		public AncientWatch(TimeOfDay timeOfDay)
		{
			this.timeOfDay = timeOfDay;

			hourSnapTimer = Components.Add(new SingleTimer());
			hourSnapTimer.Tick = t =>
			{
				hourAngle = Utilities.Lerp(hourSnapStart, hourSnapEnd, t);
			};
		}

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "AncientWatch_Casing.obj");

			var texture = ContentCache.GetTexture("AncientWatch.png");
			var atlas = ContentCache.GetAtlas("Atlas.txt");

			outerRing = CreateModel(scene, "AncientWatch_OuterRing.obj");
			mainFace = CreateSprite(scene, texture, atlas["ancient.watch.main.face"], Alignments.Center, true,
				new vec3(0, -0.048f, 0));
			mainFace.IsShadowCaster = false;
			phaseFace = CreateSprite(scene, texture, atlas["ancient.watch.phase.face"], Alignments.Center, true,
				new vec3(0, -0.047f, 0));
			phaseFace.IsShadowCaster = false;

			base.Initialize(scene, data);
		}

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			if (!accessor.GetFloat("hour.snap.time", PropertyConstraints.Positive, out var time, out message, this))
			{
				return false;
			}

			hourSnapTimer.Duration = time;

			return true;
		}

		public override void Update()
		{
			var qBase = orientation * quat.FromAxisAngle(-Constants.PiOverTwo, vec3.UnitX);
			var newHour = timeOfDay.CurrentAncientHour;

			if (newHour != oldHour)
			{
				hourSnapStart = Constants.TwoPi / 8 * oldHour;
				hourSnapEnd = Constants.TwoPi / 8 * newHour;
				hourSnapTimer.IsPaused = false;

				oldHour = newHour;
			}

			var angleOuter = -Constants.TwoPi / 64 * timeOfDay.CurrentAncientMinute;
			var qOuter = orientation * quat.FromAxisAngle(angleOuter, vec3.UnitY);
			var qHour = qBase * quat.FromAxisAngle(hourAngle, -vec3.UnitZ);

			outerRing.Orientation.SetValue(qOuter, true);
			mainFace.Orientation.SetValue(qHour, true);
			phaseFace.Orientation.SetValue(qBase, true);

			base.Update();
		}
	}
}
