﻿using Engine.Entities;

namespace Rex.Entities.Tools
{
	public abstract class Tool : Entity
	{
		protected Tool() : base((uint)EntityGroups.Tool)
		{
			IsUpdateEnabled = false;
			IsDrawEnabled = false;
		}
	}
}
