﻿using Engine;
using Engine.Core._2D;
using Engine.Core._3D;
using Engine.Entities;
using GlmSharp;
using Newtonsoft.Json.Linq;

namespace Rex.Entities.Tools
{
	public class PocketWatch : Tool
	{
		private TimeOfDay timeOfDay;
		private Sprite3D hourHand;
		private Sprite3D minuteHand;

		public PocketWatch(TimeOfDay timeOfDay)
		{
			this.timeOfDay = timeOfDay;
		}

		public override void Initialize(Scene scene, JToken data)
		{
			CreateModel(scene, "PocketWatch.obj");

			var texture = ContentCache.GetTexture("PocketWatch.png");
			var atlas = ContentCache.GetAtlas("Atlas.txt");

			hourHand = CreateSprite(scene, texture, atlas["hour.hand"], new ivec2(15, 67), true,
				new vec3(0, 0.08f, 0));
			hourHand.IsShadowCaster = false;
			minuteHand = CreateSprite(scene, texture, atlas["minute.hand"], new ivec2(9, 77), true,
				new vec3(0, 0.1f, 0));
			minuteHand.IsShadowCaster = false;

			base.Initialize(scene, data);
		}

		public override void Update()
		{
			var angle1 = Constants.TwoPi / 12 * timeOfDay.CurrentHour;
			var angle2 = Constants.TwoPi / 60 * timeOfDay.CurrentMinute;
			var qBase = orientation * quat.FromAxisAngle(-Constants.PiOverTwo, vec3.UnitX);
			var q1 = qBase * quat.FromAxisAngle(angle1, -vec3.UnitZ);
			var q2 = qBase * quat.FromAxisAngle(angle2, -vec3.UnitZ);

			hourHand.Orientation.SetValue(q1, true);
			minuteHand.Orientation.SetValue(q2, true);

			base.Update();
		}
	}
}
