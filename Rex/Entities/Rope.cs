﻿using System;
using System.Linq;
using Engine;
using Engine.Animation;
using Engine.Core;
using Engine.Entities;
using Engine.Physics.Verlet;
using Engine.Shapes._3D;
using Engine.Utility;
using GlmSharp;
using Newtonsoft.Json.Linq;
using Rex.Physics;

namespace Rex.Entities
{
	public class Rope : Entity
	{
		private Box box;
		private Bone[] bones;
		private RigidRope rope;

		private float radius;

		public Rope() : base((uint)EntityGroups.Tool)
		{
			box = new Box(20, 2, 10, BoxFlags.None);
			//IsUpdateEnabled = false;
			//IsDrawEnabled = false;
		}

		public RigidRope InnerRope => rope;

		public override void Initialize(Scene scene, JToken data)
		{
			const float SegmentLength = 1;

			// It's assumed that the rope will be cut using length-one segments.
			var mesh = ContentCache.GetMesh("Rope.obj");
			var bounds = mesh.Bounds;

			radius = bounds.y;

			var ropeLength = bounds.z;
			var boneCount = (int)(ropeLength / SegmentLength) + 1;
			var points = mesh.Points;
			var vertices = mesh.Vertices;
			var boneIndexes = new ivec2[vertices.Length];
			var boneWeights = new vec2[vertices.Length];

			for (int i = 0; i < vertices.Length; i++)
			{
				var z = points[vertices[i].x].z;

				boneIndexes[i] = new ivec2((int)Math.Round((z + ropeLength / 2) / SegmentLength), 0);
				boneWeights[i] = new vec2(1, 0);
			}

			mesh.BoneIndexes = boneIndexes;
			mesh.BoneWeights = boneWeights;

			var pose = new vec3[boneCount];

			for (int i = 0; i < pose.Length; i++)
			{
				pose[i] = new vec3(0, 4, -ropeLength / 2 + SegmentLength * i);
			}

			var skeleton = CreateSkeleton(scene, mesh, pose);

			bones = skeleton.Bones;
			rope = new RigidRope(SegmentLength, pose);

			base.Initialize(scene, data);

			new RopeTester(this);
		}
		
		public override void Update()
		{
			rope.Update();

			var points = rope.Points;

			// Apply physics.
			/*
			foreach (var p in points)
			{
				var v = p.Position - p.OldPosition;

				if (PhysicsHelper.Detect(box, p.Position, v, radius, out var result))
				{
					p.Position += result;
				}
			}
			*/

			// Update bone transforms.
			for (int i = 0; i < bones.Length; i++)
			{
				vec3 p0;
				vec3 p1;

				if (i == 0)
				{
					p0 = points[0].Position;
					p1 = points[1].Position;
				}
				else if (i == bones.Length - 1)
				{
					p0 = points[points.Length - 1].Position;
					p1 = points.Last().Position;
				}
				else
				{
					p0 = points[i - 1].Position;
					p1 = points[i + 1].Position;
				}

				var v = p1 - p0;
				var bone = bones[i];
				bone.Position.SetValue(points[i].Position, true);
				bone.Orientation.SetValue(Utilities.LengthSquared(v) < 0.00001f
					? quat.Identity
					: Utilities.Orientation(vec3.UnitZ, v), true);
			}

			base.Update();
		}

		public override void Draw()
		{
			var primitives = Scene.Primitives;

			foreach (var p in rope.Points)
			{
				var sphere = new Sphere(radius);
				sphere.Position = p.Position;

				primitives.Draw(sphere, Color.White);
			}

			primitives.Draw(box, Color.Orange);
		}
	}
}
