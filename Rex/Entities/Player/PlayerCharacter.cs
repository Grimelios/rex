﻿using Engine;
using Engine.Entities;
using Engine.Graphics._3D;
using Engine.Input;
using Engine.Input.Data;
using Engine.Props;
using GlmSharp;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Newtonsoft.Json.Linq;
using Rex.Entities.Tools;
using Rex.Physics;
using Rex.View;

namespace Rex.Entities.Player
{
	public class PlayerCharacter : Entity, IControllable
	{
		private PlayerData playerData;
		private PlayerControls controls;
		private PlayerController controller;
		private TimeOfDay timeOfDay;
		private FirstPersonView firstPersonView;
		private PocketWatch pocketWatch;
		private Rope rope;
		private RopeTool ropeTool;

		private PlayerStates state;

		public PlayerCharacter(TimeOfDay timeOfDay, FirstPersonView firstPersonView) : base((uint)EntityGroups.Player)
		{
			this.timeOfDay = timeOfDay;
			this.firstPersonView = firstPersonView;

			playerData = new PlayerData();
			controls = new PlayerControls();
			controller = new PlayerController(this, playerData, controls, firstPersonView);
			state = PlayerStates.None;

			InputProcessor.Add(this, 20);
		}

		public bool IsJumpAvailable => (state & PlayerStates.Airborne) == 0;
		public float Yaw => firstPersonView.Yaw;

		public PlayerStates State => state;

		public Mesh Map { get; set; }

		public override void Initialize(Scene scene, JToken data)
		{
			pocketWatch = new PocketWatch(timeOfDay);
			pocketWatch.Initialize(scene, null);

			var accessor = Properties.Access();
			var radius = accessor.GetFloat("player.radius");
			var height = accessor.GetFloat("player.height");

			CreateBody(scene, new CylinderShape(height, radius), (uint)PhysicsGroups.Player, RigidBodyTypes.Kinematic);

			rope = scene.Add(new Rope());
			ropeTool = new RopeTool(rope);

			var verlet = rope.InnerRope.Points;

			/*
			verlet[0].IsFixed = true;
			verlet[0].Position = new vec3(-1, 1.25f, -3);
			verlet[1].IsFixed = true;
			*/

			verlet[0].IsFixed = true;
			//rope.InnerRope.VisiblePointCount = verlet.Length;

			base.Initialize(scene, data);
		}

		public InputFlowTypes ProcessInput(FullInputData data)
		{
			controller.ProcessInput(data);

			return InputFlowTypes.BlockingUnpaused;
		}

		public void Jump()
		{
			var v = Velocity;
			v.y = playerData.JumpSpeed;
			Velocity = v;

			state |= PlayerStates.Airborne | PlayerStates.Jumping;
			controllingBody.IsAffectedByGravity = true;
		}

		public void LimitJump()
		{
			state &= ~PlayerStates.Jumping;
			state |= PlayerStates.JumpDecelerating;

			JumpDecelerate();
		}

		private void JumpDecelerate()
		{
			var v = Velocity;
			v.y -= playerData.JumpDeceleration * Game.DeltaTime;

			if (v.y <= playerData.JumpLimit)
			{
				v.y = playerData.JumpLimit;
				state &= ~PlayerStates.JumpDecelerating;
			}

			Velocity = v;
		}

		public override void Update()
		{
			if ((state & PlayerStates.JumpDecelerating) > 0)
			{
				JumpDecelerate();
			}

			base.Update();

			firstPersonView.Eye = position + new vec3(0, playerData.EyeHeight, 0);
			ropeTool.Refresh(this);
		}
	}
}
