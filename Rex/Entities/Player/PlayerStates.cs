﻿using System;

namespace Rex.Entities.Player
{
	[Flags]
	public enum PlayerStates
	{
		Airborne = 1<<0,
		JumpDecelerating = 1<<1,
		Jumping = 1<<2,
		None = 0,
		Running = 1<<3
	}
}
