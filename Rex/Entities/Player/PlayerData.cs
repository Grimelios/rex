﻿using Engine.Interfaces;
using Engine.Props;

namespace Rex.Entities.Player
{
	public class PlayerData : IReloadable
	{
		public PlayerData()
		{
			Properties.Access(this);
		}

		public float RunAcceleration { get; private set; }
		public float RunDeceleration { get; private set; }
		public float RunMaxSpeed { get; private set; }
		public float JumpSpeed { get; private set; }
		public float JumpLimit { get; private set; }
		public float JumpDeceleration { get; private set; }
		public float EyeHeight { get; private set; }

		public bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new[]
			{
				"player.run.acceleration",
				"player.run.deceleration",
				"player.run.max.speed",
				"player.jump.speed",
				"player.jump.limit",
				"player.jump.deceleration",
				"player.eye.height"
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			RunAcceleration = results["player.run.acceleration"];
			RunDeceleration = results["player.run.deceleration"];
			RunMaxSpeed = results["player.run.max.speed"];
			JumpSpeed = results["player.jump.speed"];
			JumpLimit = results["player.jump.limit"];
			JumpDeceleration = results["player.jump.deceleration"];
			EyeHeight = results["player.eye.height"];

			return true;
		}
	}
}
