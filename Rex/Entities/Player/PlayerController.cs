﻿using Engine.Input.Data;
using Engine.Utility;
using GlmSharp;
using Rex.View;

namespace Rex.Entities.Player
{
	public class PlayerController
	{
		private PlayerData playerData;
		private PlayerCharacter player;
		private PlayerControls controls;
		private FirstPersonView firstPersonView;

		public PlayerController(PlayerCharacter player, PlayerData playerData, PlayerControls controls,
			FirstPersonView firstPersonView)
		{
			this.player = player;
			this.playerData = playerData;
			this.controls = controls;
			this.firstPersonView = firstPersonView;
		}

		public void ProcessInput(FullInputData data)
		{
			firstPersonView.ProcessInput(data);

			ProcessRun(data);
			ProcessJump(data);
		}

		private void ProcessRun(FullInputData data)
		{
			bool forward = data.Query(controls.RunForward, InputStates.Held);
			bool back = data.Query(controls.RunBack, InputStates.Held);
			bool left = data.Query(controls.StrafeLeft, InputStates.Held);
			bool right = data.Query(controls.StrafeRight, InputStates.Held);

			var acceleration = vec2.Zero;

			if (forward ^ back)
			{
				acceleration.y = forward ? -1 : 1;
			}

			if (left ^ right)
			{
				acceleration.x = left ? -1 : 1;
			}

			var v = player.Velocity;
			var a = Utilities.Rotate(acceleration * playerData.RunAcceleration, firstPersonView.Yaw);
			var vNew = Utilities.ModifyVelocity(player.Velocity.swizzle.xz, a,  playerData.RunDeceleration,
				playerData.RunMaxSpeed);

			player.Velocity = new vec3(vNew.x, v.y, vNew.y);
		}

		private void ProcessJump(FullInputData data)
		{
			if (player.IsJumpAvailable)
			{
				if (data.Query(controls.Jump, InputStates.PressedThisFrame))
				{
					player.Jump();
				}
			}
			else if ((player.State & PlayerStates.Jumping) > 0 &&
				player.ControllingBody.LinearVelocity.Y > playerData.JumpLimit)
			{
				player.LimitJump();
			}
		}
	}
}
