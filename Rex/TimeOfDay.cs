﻿using System;
using Engine;
using Engine.Interfaces;

namespace Rex
{
	public class TimeOfDay : IDynamic
	{
		private const float DayLength = 20 * 60;
		private const float HourLength = DayLength / 24;
		private const float MinuteLength = HourLength / 60;
		private const float AncientHourLength = DayLength / 16;
		private const float AncientMinuteLength = AncientHourLength / 64;
		
		public float CurrentTime { get; private set; }
		public float CurrentHour => (int)Math.Floor(CurrentTime / HourLength) + CurrentMinute / 60;
		public float CurrentMinute => CurrentTime / MinuteLength % 60;
		public int CurrentAncientHour => (int)Math.Floor(CurrentTime / AncientHourLength);
		public float CurrentAncientMinute => CurrentTime / AncientMinuteLength % 64;

		public void Update()
		{
			CurrentTime += Game.DeltaTime;

			if (CurrentTime >= DayLength)
			{
				CurrentTime -= DayLength;
			}
		}
	}
}
