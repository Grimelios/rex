﻿using GlmSharp;

namespace Rex
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			var x = int.Parse(args[0]);// + 1920;
			var y = int.Parse(args[1]);

			var game = new MainGame(new ivec2(x, y));
			game.Run();
		}
	}
}
