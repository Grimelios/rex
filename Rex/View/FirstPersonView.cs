﻿using Engine.View;
using GlmSharp;

namespace Rex.View
{
	public class FirstPersonView : OrbitView
	{
		public vec3 Eye
		{
			set => Camera.Position.SetValue(value, true);
		}

		public override void Update()
		{
			Camera.Orientation.SetValue(quat.FromAxisAngle(-Pitch, vec3.UnitX) * quat.FromAxisAngle(Yaw, vec3.UnitY),
				true);
		}
	}
}
