﻿using System;
using GlmSharp;

namespace Engine.Shapes._3D
{
	public class Point3D : Shape3D
	{
		public Point3D() : this(vec3.Zero)
		{
		}

		public Point3D(vec3 p) : base(ShapeTypes3D.Point, false)
		{
			Position = p;
		}

		// This is unlikely to ever be used.
		public override bool Contains(vec3 p, Coordinates coords)
		{
			if (coords == Coordinates.Absolute)
			{
				p -= Position;
			}

			return p == vec3.Zero;
		}
	}
}
