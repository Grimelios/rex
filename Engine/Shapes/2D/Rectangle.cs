﻿using System;
using System.Diagnostics;
using Engine.Utility;
using GlmSharp;

namespace Engine.Shapes._2D
{
	public class Rectangle : Shape2D
	{
		public Rectangle() : this(0, 0, 0, 0)
		{
		}

		public Rectangle(float size) : this(0, 0, size, size)
		{
		}

		public Rectangle(float width, float height) : this(0, 0, width, height)
		{
		}

		public Rectangle(float x, float y, float size) : this(x, y, size, size)
		{
		}

		public Rectangle(float x, float y, float width, float height) : base(ShapeTypes2D.Rectangle)
		{
			X = x;
			Y = y;
			Width = width;
			Height = height;
		}

		public float Width { get; set; }
		public float Height { get; set; }

		public float Left
		{
			get => position.x - Width / 2;
			set => position.x = value + Width / 2;
		}

		public float Right
		{
			get => position.x + Width / 2;
			set => position.x = value - Width / 2;
		}

		public float Top
		{
			get => position.y - Height / 2;
			set => position.y = value + Height / 2;
		}

		public float Bottom
		{
			get => position.y + Height / 2;
			set => position.y = value - Height / 2;
		}

		public vec2 Dimensions => new vec2(Width, Height);

		public vec2[] Corners
		{
			get
			{
				var halfWidth = Width / 2;
				var halfHeight = Height / 2;

				var points = new []
				{
					new vec2(-halfWidth, -halfHeight),
					new vec2(halfWidth, -halfHeight),
					new vec2(halfWidth, halfHeight),
					new vec2(-halfWidth, halfHeight)
				};

				if (Rotation != 0)
				{
					var matrix = Utilities.RotationMatrix2D(Rotation);

					for (int i = 0; i < points.Length; i++)
					{
						points[i] = matrix * points[i];
					}
				}

				for (int i = 0; i < points.Length; i++)
				{
					points[i] += position;
				}

				return points;
			}
		}

		public Rectangle Clone(float scale)
		{
			var result = new Rectangle(X * scale, Y * scale, Width * scale, Height * scale);
			result.Rotation = Rotation;

			return result;
		}

		public Rectangle Expand(float amount)
		{
			Debug.Assert(amount > 0, "Rectangle expansion must be positive.");

			return new Rectangle(position.x - amount, position.y - amount, Width + amount * 2, Height + amount * 2);
		}

		public override bool Contains(vec2 p)
		{
			if (Rotation != 0)
			{
				p = Utilities.Rotate(p - Position, -Rotation);
			}

			var dX = Math.Abs(position.x - p.x);
			var dY = Math.Abs(position.y - p.y);

			return dX <= Width / 2 && dY <= Height / 2;
		}
	}
}
