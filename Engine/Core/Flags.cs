﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Core
{
	// This class should be constrained to enums, but that's not currently possible in C# (at least not simply).
	public class Flags<T> where T : struct
	{
		public Flags(T value)
		{
			Value = value;
		}

		public T Value { get; set; }
	}
}
