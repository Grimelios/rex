﻿using System;
using System.Diagnostics;
using Engine.Interfaces;
using Engine.Utility;
using GlmSharp;

namespace Engine.Physics.Verlet
{
	public class VerletRope3D: IDynamic
	{
		private float damping;
		private float gravity;
		private float segmentLength;

		private VerletPoint3D[] points;

		private int visiblePointCount;

		public VerletRope3D(float segmentLength, float damping, float gravity, int points) :
			this(segmentLength, damping, gravity, null)
		{
			this.points = new VerletPoint3D[points];

			for (int i = 0; i < points; i++)
			{
				this.points[i] = new VerletPoint3D(1);
			}
		}

		// This is useful when extending or shooting the rope out (such that not all rope points are visible at once).
		public int VisiblePointCount
		{
			get => visiblePointCount;
			set
			{
				Debug.Assert(value >= 0 && value <= points.Length, "Invalid rope visible point count.");

				visiblePointCount = value;
			}
		}

		public float SegmentLength => segmentLength;

		public VerletRope3D(float segmentLength, float damping, float gravity, vec3[] points)
		{
			this.damping = damping;
			this.gravity = gravity;
			this.segmentLength = segmentLength;

			if (points != null)
			{
				this.points = new VerletPoint3D[points.Length];

				for (int i = 0; i < points.Length; i++)
				{
					this.points[i] = new VerletPoint3D(points[i], 1);
				}
			}
		}

		public VerletPoint3D[] Points => points;

		// TODO: Verify that ropes behave the same at varying framerates.
		public void Update()
		{
			for (int i = 0; i < VisiblePointCount; i++)
			{
				var point = points[i];

				if (point.IsFixed)
				{
					continue;
				}

				var p = point.Position;
				var temp = p;

				p.y += gravity * Game.DeltaTime;
				p += (p - point.OldPosition) * damping;
				point.Position = p;
				point.OldPosition = temp;
			}

			SolveConstraints();
		}

		private void SolveConstraints()
		{
			const int Iterations = 8;
			const float K = 0.99f;

			for (int i = 0; i < Iterations; i++)
			{
				var forces = new vec3[VisiblePointCount - 1];

				for (int j = 0; j < VisiblePointCount - 1; j++)
				{
					var point1 = points[j];
					var point2 = points[j + 1];

					if (point1.IsFixed && point2.IsFixed)
					{
						continue;
					}

					var p1 = point1.Position;
					var p2 = point2.Position;
					var squared = Utilities.DistanceSquared(p1, p2);

					if (squared > segmentLength * segmentLength)
					{
						var d = (float)Math.Sqrt(squared);
						var delta = d - segmentLength;

						forces[j] = (p2 - p1) / d * delta * K;
					}
				}

				for (int j = 0; j < VisiblePointCount - 1; j++)
				{
					var point1 = points[j];
					var point2 = points[j + 1];

					if (point1.IsFixed && point2.IsFixed)
					{
						continue;
					}

					var p1 = point1.Position;
					var p2 = point2.Position;
					var f = forces[j];

					if (point1.IsFixed)
					{
						p2 -= f;
					}
					else if (point2.IsFixed)
					{
						p1 += f;
					}
					else
					{
						var ratio = point1.Mass / (point1.Mass + point2.Mass);

						p1 += f * ratio;
						p2 -= f * (1 - ratio);
					}

					point1.Position = p1;
					point2.Position = p2;
				}
			}

			// Compute rotations (right-handed).
			for (int i = 1; i < VisiblePointCount - 1; i++)
			{
				points[i].Orientation = Utilities.Orientation(points[i - 1].Position, points[i + 1].Position);
			}
		}
	}
}
