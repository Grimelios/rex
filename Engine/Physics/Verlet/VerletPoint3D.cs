﻿using System.Diagnostics;
using Engine.Interfaces._3D;
using GlmSharp;

namespace Engine.Physics.Verlet
{
	[DebuggerDisplay("P={Position}, Old={OldPosition}, Orientation={Orientation}, Mass={Mass}")]
	public class VerletPoint3D: IPositionable3D, IOrientable
	{
		private float mass;

		public VerletPoint3D(float mass, bool isFixed = false) : this(vec3.Zero, mass, isFixed)
		{
		}

		public VerletPoint3D(vec3 p, float mass, bool isFixed = false)
		{
			Debug.Assert(mass > 0, "Verlet point mass must be positive.");

			Position = p;
			OldPosition = p;
			Mass = mass;
			IsFixed = isFixed;
		}

		public vec3 Position { get; set; }
		public vec3 OldPosition { get; internal set; }
		public quat Orientation { get; set; }

		public float Mass
		{
			get => mass;
			set
			{
				Debug.Assert(value > 0, "Verlet point mass must be positive.");

				mass = value;
			}
		}

		public bool IsFixed { get; set; }
	}
}
