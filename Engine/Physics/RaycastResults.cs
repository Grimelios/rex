﻿using GlmSharp;
using Jitter.Dynamics;

namespace Engine.Physics
{
	public class RaycastResults
	{
		public RaycastResults(vec3 p, vec3 n, vec3[] triangle, ivec3 indexes, float t,
			int material = -1) :
			this(null, p, n, triangle, indexes, t, material)
		{
		}

		public RaycastResults(RigidBody body, vec3 p, vec3 n, float t, int material = -1) :
			this(body, p, n, null, ivec3.Zero, t, material)
		{
		}

		public RaycastResults(RigidBody body, vec3 p, vec3 n, vec3[] triangle, ivec3 indexes, float t,
			int material = -1)
		{
			Body = body;
			Position = p;
			Normal = n;
			Triangle = triangle;
			Indexes = indexes;
			T = t;
			Material = material;
		}

		public RigidBody Body { get; }

		public vec3 Position { get; }
		public vec3 Normal { get; }
		public vec3[] Triangle { get; }

		// These are the indexes of the three triangle points (optionally populated for triangle raycasts).
		public ivec3 Indexes { get; }

		public float T { get; }
		public int Material { get; }
	}
}
