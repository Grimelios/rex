﻿using Engine.Interfaces._3D;
using GlmSharp;

namespace Engine.Lighting
{
	public class Spotlight : LightSource
	{
		public float Spread { get; set; }
	}
}
