﻿using Engine.Core._3D;
using GlmSharp;

namespace Engine.Interfaces._3D
{
	public interface IRenderOrientable
	{
		RenderOrientationField Orientation { get; }

		void Recompute(float t);
	}
}
