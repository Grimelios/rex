﻿using Engine.Props;

namespace Engine.Interfaces
{
	// TODO: Should reloadable objects also be disposable?
	public interface IReloadable
	{
		bool Reload(PropertyAccessor accessor, out string message);
	}
}
