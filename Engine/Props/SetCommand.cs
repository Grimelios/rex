﻿using System;
using System.Collections.Generic;
using System.Linq;
using Engine.Editing;

namespace Engine.Props
{
	internal class SetCommand : TerminalCommand
	{
		private Dictionary<string, string> map;
		private PropertyAccessor accessor;
		private string[] sortedKeys;

		public SetCommand(Dictionary<string, string> map, PropertyAccessor accessor, string[] sortedKeys) : base("set")
		{
			this.map = map;
			this.accessor = accessor;
			this.sortedKeys = sortedKeys;

			sortedKeys = map.Keys.ToArray();

			Array.Sort(sortedKeys);
		}

		public override TerminalArgument[] Usage => new[]
		{
			new TerminalArgument("property", ArgumentTypes.Required),
			new TerminalArgument("value", ArgumentTypes.Required)
		};

		public override string[] GetOptions(string[] args)
		{
			// Property values aren't autocompleted.
			return args.Length == 0 ? sortedKeys : null;
		}

		public override bool Process(string[] args, out string message)
		{
			var key = args[0];

			if (!map.ContainsKey(key))
			{
				message = $"Unknown property '{key}'.";

				return false;
			}

			var tracker = accessor.Tracker;
			var matchedProperty = tracker.Keys.FirstOrDefault(prop => prop.Key == key);

			if (matchedProperty.Key == null)
			{
				message = $"Property '{key}' is untracked.";

				return false;
			}

			var targets = tracker[matchedProperty];
			var value = args[1];

			map[key] = value;

			foreach (var target in targets)
			{
				if (!target.Reload(accessor, out message))
				{
					return false;
				}
			}

			message = $"Property '{key}' modified.";

			return true;
		}
	}
}
