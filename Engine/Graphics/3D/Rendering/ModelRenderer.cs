﻿using Engine.Core._3D;
using Engine.Lighting;
using Engine.Shaders;
using Engine.View;
using static Engine.GL;

namespace Engine.Graphics._3D.Rendering
{
	public class ModelRenderer : MeshRenderer<Model>
	{
		public ModelRenderer(Camera3D camera, GlobalLight light) : base(camera, light, "model")
		{
			shader = new Shader();
			shader.Attach(ShaderTypes.Vertex, "ModelShadow.vert");
			shader.Attach(ShaderTypes.Fragment, "ModelShadow.frag");
			shader.AddAttribute<float>(3, GL_FLOAT);
			shader.AddAttribute<float>(2, GL_FLOAT);
			shader.AddAttribute<float>(1, GL_FLOAT);
			shader.AddAttribute<float>(3, GL_FLOAT);

			Bind(bufferId, indexId);
		}

		protected override float[] GetData(Mesh mesh)
		{
			var points = mesh.Points;
			var sources = mesh.Sources;
			var normals = mesh.Normals;
			var vertices = mesh.Vertices;
			var buffer = new float[vertices.Length * 9];

			for (int i = 0; i < vertices.Length; i++)
			{
				var v = vertices[i];
				var p = points[v.x];
				var s = sources[v.y];
				var n = normals[v.w];

				int start2 = i * 9;

				buffer[start2] = p.x;
				buffer[start2 + 1] = p.y;
				buffer[start2 + 2] = p.z;
				buffer[start2 + 3] = s.x;
				buffer[start2 + 4] = s.y;
				buffer[start2 + 5] = v.z;
				buffer[start2 + 6] = n.x;
				buffer[start2 + 7] = n.y;
				buffer[start2 + 8] = n.z;
			}

			return buffer;
		}
	}
}
