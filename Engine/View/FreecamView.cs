﻿using System;
using System.Diagnostics;
using Engine.Input;
using Engine.Input.Data;
using Engine.Interfaces;
using Engine.Props;
using Engine.Utility;
using GlmSharp;
using static Engine.GLFW;

namespace Engine.View
{
	public class FreecamView : OrbitView
	{
		private vec3 velocity;
		private vec3 acceleration;

		public float Acceleration { get; set; }
		public float Deceleration { get; set; }
		public float MaxSpeed { get; set; }

		public override bool Reload(PropertyAccessor accessor, out string message)
		{
			var keys = new []
			{
				"freecam.acceleration",
				"freecam.deceleration",
				"freecam.max.speed",
				"freecam.max.pitch",
				"freecam.sensitivity",
			};

			if (!accessor.GetFloats(keys, PropertyConstraints.Positive, out var results, out message, this))
			{
				return false;
			}

			Acceleration = results["freecam.acceleration"];
			Deceleration = results["freecam.deceleration"];
			MaxSpeed = results["freecam.max.speed"];
			MaxPitch = results["freecam.max.pitch"];
			Sensitivity = results["freecam.sensitivity"];

			return true;
		}

		public override void ProcessInput(FullInputData data)
		{
			// Orientation changes are processed before moving the freecam.
			base.ProcessInput(data);

			// These binds are hardcoded since the freecam is meant to be an internal development tool (rather than
			// something used by players).
			bool forward = data.Query(InputTypes.Keyboard, GLFW_KEY_W, InputStates.Held);
			bool back = data.Query(InputTypes.Keyboard, GLFW_KEY_S, InputStates.Held);
			bool left = data.Query(InputTypes.Keyboard, GLFW_KEY_A, InputStates.Held);
			bool right = data.Query(InputTypes.Keyboard, GLFW_KEY_D, InputStates.Held);
			bool up = data.Query(InputTypes.Keyboard, GLFW_KEY_E, InputStates.Held);
			bool down = data.Query(InputTypes.Keyboard, GLFW_KEY_Q, InputStates.Held);

			acceleration = vec3.Zero;
			acceleration.x = left ^ right ? (left ? -1 : 1) : 0;
			acceleration.y = up ^ down ? (down ? -1 : 1) : 0;
			acceleration.z = forward ^ back ? (forward ? -1 : 1) : 0;
		}

		public override void Update()
		{
			// This assertion was added after I spent maybe 20 minutes trying to figure out why the freecam wasn't
			// moving. Acceleration or decleration could be checked for the same result.
			Debug.Assert(MaxSpeed > 0, "Freecam maximum speed must be positive (this likely means that acceleration " +
				"values were never assigned).");

			int Sign(vec3 v)
			{
				return Math.Sign(v.x != 0 ? v.x : (v.y != 0 ? v.y : v.z));
			}

			var q = quat.FromAxisAngle(-Pitch, vec3.UnitX) * quat.FromAxisAngle(Yaw, vec3.UnitY);

			if (acceleration != vec3.Zero)
			{
				var result = q.Inverse * new vec3(acceleration.x, 0, acceleration.z) + new vec3(0, acceleration.y, 0);

				velocity += Utilities.Normalize(result) * Acceleration * Game.DeltaTime;

				if (Utilities.LengthSquared(velocity) > MaxSpeed * MaxSpeed)
				{
					velocity = Utilities.Normalize(velocity) * MaxSpeed;
				}
			}
			else if (Utilities.LengthSquared(velocity) > 0)
			{
				var sign = Sign(velocity);
				velocity -= Utilities.Normalize(velocity) * Deceleration * Game.DeltaTime;

				if (sign != Sign(velocity))
				{
					velocity = vec3.Zero;
				}
			}

			Camera.Position.SetValue(Camera.Position.Value + velocity * Game.DeltaTime, true);
			Camera.Orientation.SetValue(q, true);
		}
	}
}
