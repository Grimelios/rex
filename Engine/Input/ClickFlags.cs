﻿using System;

namespace Engine.Input
{
	[Flags]
	public enum ClickFlags
	{
		None = 0,
		WasClicked = 1<<0,
		WasHovered = 1<<1
	}
}
