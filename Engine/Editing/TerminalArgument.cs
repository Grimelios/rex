﻿using System.Diagnostics;

namespace Engine.Editing
{
	public class TerminalArgument
	{
		public TerminalArgument(string value, ArgumentTypes type)
		{
			Debug.Assert(!string.IsNullOrEmpty(value), "Argument value can't be null or empty.");

			Value = value;
			Type = type;
		}

		public string Value { get; }

		public ArgumentTypes Type { get; }
	}
}
