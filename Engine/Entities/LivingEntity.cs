﻿using System;
using System.Diagnostics;
using Engine.Interfaces;
using GlmSharp;

namespace Engine.Entities
{
	public abstract class LivingEntity : Entity, ITargetable
	{
		private int health;
		private int maxHealth;

		protected LivingEntity(uint group) : base(group)
		{
		}

		public int Health
		{
			get => health;
			set
			{
				Debug.Assert(value <= maxHealth, "Health can't be greater than max health.");

				if (health != value)
				{
					var oldHealth = health;
				
					health = value;
					OnHealthChange(oldHealth, health);
				}
			}
		}

		public int MaxHealth
		{
			get => maxHealth;
			set
			{
				Debug.Assert(value > 0, "Max health must be positive.");

				if (maxHealth != value)
				{
					var oldMax = maxHealth;

					maxHealth = value;
					OnMaxHealthChange(oldMax, maxHealth);
				}
			}
		}

		public virtual void OnDamage(int damage, object source = null)
		{
			OnHit(damage, 0, vec3.Zero, vec3.Zero, source);
		}

		// Note that the position used here is in world space (not relative to the entity).
		public virtual void OnHit(int damage, int knockback, vec3 p, vec3 direction, object source = null)
		{
			// TODO: Should the function return early if health is already zero?
			Health = Math.Max(health - damage, 0);
			
			if (health == 0)
			{
				OnDeath(p, direction * knockback);
			}
		}

		protected virtual void OnHealthChange(int oldHealth, int newHealth)
		{
		}

		protected virtual void OnMaxHealthChange(int oldMax, int newMax)
		{
		}

		// TODO: Are other death values needed? (i.e. other values from the final hit)
		// These values allow entities to react to the final hit that killed them (e.g. an enemy swapping to a
		// ragdoll).
		protected virtual void OnDeath(vec3 p, vec3 force)
		{
		}
	}
}
