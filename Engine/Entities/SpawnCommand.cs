﻿using System;
using Engine.Editing;

namespace Engine.Entities
{
	public class SpawnCommand : TerminalCommand
	{
		private const string Path = "Zeldo.Entities.";

		private Scene scene;

		public SpawnCommand(Scene scene) : base("spawn")
		{
			this.scene = scene;
		}

		public override TerminalArgument[] Usage => new []
		{
			new TerminalArgument("type", ArgumentTypes.Required),
		};

		public override string[] GetOptions(string[] args)
		{
			// TODO: Retrieve and store available entities to be spawned (might need to update entities to account for spawning from the terminal).
			return null;
		}

		public override bool Process(string[] args, out string result)
		{
			if (args.Length != 1)
			{
				result = "Usage: spawn *class* (e.g. 'spawn Objects.Crate')";

				return false;
			}

			var raw = args[0];
			var type = Type.GetType(Path + args[0]);

			if (type == null)
			{
				result = $"Unknown entity '{Path}{raw}'.";

				return false;
			}

			result = $"Spawned '{Path}{raw}'.";

			return true;
		}
	}
}
