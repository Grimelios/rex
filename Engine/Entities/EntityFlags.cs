﻿using System;

namespace Engine.Entities
{
	[Flags]
	public enum EntityFlags
	{
		None = 0,

		IsInitialized = 1<<0,
		IsPositionSet = 1<<1,
		IsOrientationSet = 1<<2,

		IsUpdateEnabled = 1<<3,
		IsDrawEnabled = 1<<4,

		// TODO: Might want to rename this to PreserveBodies later (since ragdolls, by definition, contain multiple bodies).
		// This is useful when swapping out an actor (or other controlled entity) with a physics object or ragdoll.
		PreserveBody = 1<<5
	}
}
