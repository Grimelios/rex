﻿using System.Diagnostics;
using Engine.Interfaces;

namespace Engine.Interpolation
{
	// Interpolators are similar to timers, but I think it still makes sense to have them separate (primarily to more
	// easily modify start and end values mid-tick).
	public abstract class Interpolator<T> : IComponent
	{
		protected const string NullMessage = "Can't interpolate on a null target.";

		private bool isRepeatable;

		private float duration;
		private float elapsed;

		private EaseTypes easeType;

		protected Interpolator(T start, T end, float duration, EaseTypes easeType, bool isRepeatable)
		{
			Debug.Assert(duration >= 0, "Duration can't be negative.");

			this.duration = duration;
			this.easeType = easeType;
			this.isRepeatable = isRepeatable;
			
			Start = start;
			End = end;
		}

		// In some cases, the start and end values need to be modified mid-interpolation.
		public T Start { get; set; }
		public T End { get; set; }

		public bool IsComplete { get; private set; }
		public bool IsEnabled { get; set; }

		public float Duration
		{
			get => duration;
			set
			{
				Debug.Assert(value > 0, "Interpolator duration must be positive.");

				duration = value;
			}
		}

		public void Reset()
		{
			elapsed = 0;
			IsComplete = false;
		}

		protected abstract void Lerp(float t);

		// This can be called directly in some cases (such as for certain attacks).
		public void Interpolate(float t)
		{
			Lerp(Ease.Compute(t, easeType));
		}

		public void Update()
		{
			if (IsComplete || !IsEnabled)
			{
				return;
			}

			elapsed += Game.DeltaTime;

			if (elapsed >= duration)
			{
				// This ignores any leftover time (such that the end state is set exactly).
				Interpolate(1);

				if (isRepeatable)
				{
					Reset();
				}
				else
				{
					IsComplete = true;
				}

				return;
			}

			Interpolate(elapsed / duration);
		}
	}
}
